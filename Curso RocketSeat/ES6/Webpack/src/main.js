/*
import multFunction, { soma, sub as subFunction } from './funcoes';

console.log(multFunction(2, 2));
console.log(soma(1, 2));
console.log(subFunction(2, 1));
*/

/*
import * as funcoes from './funcoes';

console.log(funcoes.default(2, 2)); // mult
console.log(funcoes.soma(1, 2));
console.log(funcoes.sub(2, 1));


import somaFunction from './soma';

console.log(somaFunction(1, 2));
*/

/*
const minhaPromise = () => new Promise((resolve, reject) => setTimeout(() => resolve('OK'), 2000) );

minhaPromise()
    .then(response => console.log(response) )
    .catch(err => console.log(err) );

async function executaPromise() {
    console.log(await minhaPromise());
    console.log(await minhaPromise());
    console.log(await minhaPromise());
}

executaPromise();
*/


import axios from 'axios';

class Api {
    static async getUserInfo(username) {
        try {
            const response = await axios.get(`https://api.github.com/users/${username}`);
            console.log(response);
        } catch (err) {
            console.warn('Erro na API');
        }
    }
}

Api.getUserInfo('diego3g');
Api.getUserInfo('rtexa');
