"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

//-----Exercicio 1-----
console.log('-----Exercício 1-----');

var Usuario =
/*#__PURE__*/
function () {
  function Usuario(email, senha) {
    _classCallCheck(this, Usuario);

    this.email = email;
    this.senha = senha;
    this.setAdmin(false);
  }

  _createClass(Usuario, [{
    key: "setAdmin",
    value: function setAdmin(isadmin) {
      this.isadmin = isadmin;
    }
  }, {
    key: "isAdmin",
    value: function isAdmin() {
      return this.isadmin;
    }
  }]);

  return Usuario;
}();

var Admin =
/*#__PURE__*/
function (_Usuario) {
  _inherits(Admin, _Usuario);

  function Admin(email, senha) {
    var _this;

    _classCallCheck(this, Admin);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Admin).call(this, email, senha));

    _this.setAdmin(true);

    return _this;
  }

  return Admin;
}(Usuario);

var User1 = new Usuario('email@teste.com', 'senha123');
var Adm1 = new Admin('email@teste.com', 'senha123');
console.log(User1.isAdmin()); // false

console.log(Adm1.isAdmin()); // true
//-----Exercicio 2-----

console.log('-----Exercício 2-----');
var usuarios = [{
  nome: 'Diego',
  idade: 23,
  empresa: 'Rocketseat'
}, {
  nome: 'Gabriel',
  idade: 15,
  empresa: 'Rocketseat'
}, {
  nome: 'Lucas',
  idade: 30,
  empresa: 'Facebook'
}];
console.log(usuarios.map(function (item) {
  return item.idade;
}));
console.log(usuarios.filter(function (item) {
  return item.idade >= 18 && item.empresa === 'Rocketseat';
}));
console.log(usuarios.find(function (item) {
  if (item.empresa === 'Google') {
    return item;
  }
}));
console.log(usuarios.map(function (item) {
  item.idade *= 2;
  return item;
}).filter(function (item) {
  return item.idade <= 50;
})); //-----Exercicio 3-----

console.log('-----Exercício 3-----'); // 3.1

var arr = [1, 2, 3, 4, 5];
/*
arr.map(function(item) {
    return item + 10;
});
*/

arr.map(function (item) {
  return console.log(item + 10);
}); // 3.2
// Dica: Utilize uma constante pra function

var usuario = {
  nome: 'Diego',
  idade: 23
};
/*
function mostraIdade(usuario) {
    return usuario.idade;
}
*/

var mostraIdade = function mostraIdade(usuario) {
  return console.log(usuario.idade);
};

mostraIdade(usuario); // 3.3
// Dica: Utilize uma constante pra function

var nome = "Diego";
var idade = 23;
/*
function mostraUsuario(nome = 'Diego', idade = 18) {
    return { nome, idade };
}
*/

var mostraUsuario = function mostraUsuario() {
  var nome = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Diego';
  var idade = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 18;
  return console.log({
    nome: nome,
    idade: idade
  });
};

mostraUsuario(nome, idade);
mostraUsuario(nome); // 3.4

/*
const promise = function() {
    return new Promise(function(resolve, reject) {
        return resolve();
    })
}
*/

var promise = function promise() {
  return new Promise(function (resolve, reject) {
    return resolve();
  });
}; //-----Exercicio 4-----


console.log('-----Exercício 4-----');
var empresa = {
  nome4: 'Rocketseat',
  endereco4: {
    cidade4: 'Rio do Sul',
    estado4: 'SC'
  }
};
var nome4 = empresa.nome4,
    _empresa$endereco = empresa.endereco4,
    cidade4 = _empresa$endereco.cidade4,
    estado4 = _empresa$endereco.estado4;
console.log(nome4); // Rocketseat

console.log(cidade4); // Rio do Sul

console.log(estado4); // SC

/*
function mostraInfo(usuario) {
    return `${usuario.nome} tem ${usuario.idade} anos.`;
}
*/

function mostraInfo(_ref) {
  var nome = _ref.nome,
      idade = _ref.idade;
  console.log("".concat(nome, " tem ").concat(idade, " anos."));
}

mostraInfo({
  nome: 'Diego',
  idade: 23
}); //-----Exercicio 5-----

console.log('-----Exercício 5-----');
var arr2 = [1, 2, 3, 4, 5, 6];
var x = arr2[0],
    y = arr2.slice(1);
console.log(x); // 1

console.log(y); // [2, 3, 4, 5, 6]

var soma = function soma() {
  for (var _len = arguments.length, params = new Array(_len), _key = 0; _key < _len; _key++) {
    params[_key] = arguments[_key];
  }

  return params.reduce(function (total, next) {
    return total + next;
  });
};

console.log(soma(1, 2, 3, 4, 5, 6)); // 21

console.log(soma(1, 2)); // 3
//-----Exercicio 6-----

console.log('-----Exercício 6-----');
var usuario6 = 'Diego';
var idade6 = 23; //console.log('O usuário ' + usuario6 + ' possui ' + idade6 + ' anos');

console.log("O usu\xE1rio ".concat(usuario6, " possui ").concat(idade6, " anos")); //-----Exercicio 7-----

console.log('-----Exercício 7-----');
var nome7 = 'Diego';
var idade7 = 23;
var usuario7 = {
  nome7: nome7,
  idade7: idade7,
  cidade: 'Rio do Sul'
};
console.log(usuario7);
