//-----Exercicio 1-----
console.log('-----Exercício 1-----');

class Usuario {
    constructor(email, senha) {
        this.email = email;
        this.senha = senha;
        this.setAdmin(false);
    }

    setAdmin(isadmin) {
        this.isadmin = isadmin;
    }
    
    isAdmin() {
        return this.isadmin;
    }
}

class Admin extends Usuario {
    constructor(email, senha) {
        super(email, senha);
        this.setAdmin(true);
    }
}

const User1 = new Usuario('email@teste.com', 'senha123');
const Adm1 = new Admin('email@teste.com', 'senha123');
console.log(User1.isAdmin()) // false
console.log(Adm1.isAdmin()) // true


//-----Exercicio 2-----
console.log('-----Exercício 2-----');

const usuarios = [
    { nome: 'Diego', idade: 23, empresa: 'Rocketseat' },
    { nome: 'Gabriel', idade: 15, empresa: 'Rocketseat' },
    { nome: 'Lucas', idade: 30, empresa: 'Facebook' },
];

console.log (usuarios.map(item => item.idade ));

console.log (usuarios.filter(item => (item.idade >= 18 && item.empresa === 'Rocketseat')));

console.log (usuarios.find(item => {
    if (item.empresa === 'Google') {
        return item;
    }
}));


console.log (
    usuarios.map(item =>  {
        item.idade *= 2;
        return item;
    }).filter(item => item.idade <= 50));


//-----Exercicio 3-----
console.log('-----Exercício 3-----');

// 3.1
const arr = [1, 2, 3, 4, 5];
/*
arr.map(function(item) {
    return item + 10;
});
*/
arr.map(item => console.log(item + 10) );

// 3.2
// Dica: Utilize uma constante pra function
const usuario = { nome: 'Diego', idade: 23 };
/*
function mostraIdade(usuario) {
    return usuario.idade;
}
*/
const mostraIdade = usuario => console.log(usuario.idade);
mostraIdade(usuario);

// 3.3
// Dica: Utilize uma constante pra function
const nome = "Diego";
const idade = 23;
/*
function mostraUsuario(nome = 'Diego', idade = 18) {
    return { nome, idade };
}
*/
const mostraUsuario = (nome = 'Diego', idade = 18) =>  console.log({ nome, idade });
mostraUsuario(nome, idade);
mostraUsuario(nome);

// 3.4
/*
const promise = function() {
    return new Promise(function(resolve, reject) {
        return resolve();
    })
}
*/
const promise = () => new Promise((resolve, reject) => resolve());


//-----Exercicio 4-----
console.log('-----Exercício 4-----');

const empresa = {
    nome4: 'Rocketseat',
    endereco4: {
        cidade4: 'Rio do Sul',
        estado4: 'SC',
    }
};

const {nome4, endereco4: {cidade4, estado4} } = empresa;

console.log(nome4); // Rocketseat
console.log(cidade4); // Rio do Sul
console.log(estado4); // SC

/*
function mostraInfo(usuario) {
    return `${usuario.nome} tem ${usuario.idade} anos.`;
}
*/
function mostraInfo({nome, idade}) {
    console.log(`${nome} tem ${idade} anos.`);
}
mostraInfo({ nome: 'Diego', idade: 23 })


//-----Exercicio 5-----
console.log('-----Exercício 5-----');

const arr2 = [1, 2, 3, 4, 5, 6];

const [x, ...y] = arr2;

console.log(x); // 1
console.log(y); // [2, 3, 4, 5, 6]

const soma = (...params) => params.reduce((total, next) => total + next );

console.log(soma(1, 2, 3, 4, 5, 6)); // 21
console.log(soma(1, 2)); // 3


//-----Exercicio 6-----
console.log('-----Exercício 6-----');

const usuario6 = 'Diego';
const idade6 = 23;
//console.log('O usuário ' + usuario6 + ' possui ' + idade6 + ' anos');
console.log(`O usuário ${usuario6} possui ${idade6} anos`);


//-----Exercicio 7-----
console.log('-----Exercício 7-----');

const nome7 = 'Diego';
const idade7 = 23;
const usuario7 = {
    nome7,
    idade7,
    cidade: 'Rio do Sul'
};

console.log(usuario7);
