/*class List {
    constructor() {
        this.data = [];
    }

    add(data) {
        this.data.push(data);
        console.log(this.data);
    }
}

class TodoList extends List{
    constructor() {
        super();
        this.usuario = "Robson";
    }

    mostraUsuario() {
        console.log(this.usuario);
    }
}

const MinhaLista = new TodoList();

document.getElementById('novotodo').onclick = function() {
    MinhaLista.add('novo todo');
};

MinhaLista.mostraUsuario();*/



/*
const arr = [1, 3, 4, 5, 8, 9];

const newArr = arr.map(function(item) {
    return item * 2;
});

console.log(newArr);


const sum = arr.reduce((total, next) => {
    return total + next;
});

console.log(sum);


const filter = arr.filter(item => {
    return item %2 === 0;
});

console.log(filter);


const find = arr.find(item => item === 4 );

console.log(find)


const teste = () => ({ nome: "Robson"});

console.log(teste);


const soma = (a = 1, b = 2) => a + b;

console.log(soma());
console.log(soma(2, 4));
*/

//desestruturação
/*
const usuario = {
    nome: 'Robson',
    idade: '26',
    endereco: {
        cidade: 'São Carlos',
        estado: 'SP'
    }
};

//const nome = usuario.nome;
//const idade = usuario.idade;
//const cidade = usuario.endereco.cidade;

const {nome, idade, endereco: {cidade} } = usuario;

console.log(nome + "-" + idade + "-" + cidade);

function mostraNome({nome, idade}) {
    console.log(nome);
}

mostraNome(usuario);
*/

//rest

/*const usuario = {
    nome: 'Robson',
    idade: '26',
    endereco: {
        cidade: 'São Carlos',
        estado: 'SP'
    }
};

const {nome, ...resto} = usuario;

console.log(nome);
console.log(resto);

const arr = [1, 2, 3, 4];
const [a, b, ...c] = arr;
console.log(a);
console.log(b);
console.log(c);


function soma(...params) {
    return params.reduce((total, next) => total + next);
}

console.log(soma(1, 3, 4));

//spread

const arr1 = [1, 2, 3];
const arr2 = [4, 5, 6];
const arr3 = [...arr1, ...arr2];

console.log(arr3);

const usuario2 = {...usuario, nome: 'Diego'};

console.log(usuario2);
*/

// Template literals
/*
const nome = "Robson", idade = 26;

console.log('Meu nome é ' + nome + ' e tenho ' + idade + ' anos.');

console.log(`Meu nome é ${nome} e tenho ${idade} anos.`);
*/

// Object short syntax

const nome = "Robson", idade = 26;

const usuario = {
    nome,
    idade,
    empresa: 'Rocketseat'
};

console.log(usuario);