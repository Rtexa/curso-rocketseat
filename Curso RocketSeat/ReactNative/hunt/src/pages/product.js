import React, { Component } from 'react';

import { WebView } from 'react-native-webview';
/*
export default class Product extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.state.params.product.title,
        };
    };
    render() {
        return (
            <WebView></WebView>{this.props.navigation.state.params.product.url} />
        )
    }
}
*/

import { Text } from 'react-native';

//const Product = () => <Text>Product</Text>;

const Product = ({ navigation }) => (
    <WebView source={{uri: navigation.state.params.product.url}} />
);

Product.navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.product.title
});

export default Product;