var listElement = document.querySelector('#todolist');
var inputElement = document.querySelector('#todo');
var buttonElement = document.querySelector('#btn');

var todos = JSON.parse(localStorage.getItem('list_todos')) || [];

function atualizarLista() {
    listElement.innerHTML = '';
    for(todo of todos) {
        var todoElement = document.createElement('li');
        var todoText = document.createTextNode(todo);
        var linkElement = document.createElement('a');
        linkElement.setAttribute('href', '#');
        var linkText = document.createTextNode('Excluir');
        linkElement.appendChild(linkText);
        var pos = todos.indexOf(todo);
        linkElement.setAttribute('onclick', 'deleteTodo(' + pos + ')');
    
        todoElement.appendChild(todoText);
        todoElement.appendChild(linkElement);
        listElement.appendChild(todoElement);
    }
}

atualizarLista();

function adicionarTodo(todo) {
    todos.push(todo)
    atualizarLista();
    saveToStorage();
}

buttonElement.onclick = function() {
    adicionarTodo(inputElement.value);
    inputElement.value = "";
}

function deleteTodo(pos) {
    todos.splice(pos, 1);
    atualizarLista();
    saveToStorage();
}

function saveToStorage() {
    localStorage.setItem('list_todos', JSON.stringify(todos));
}