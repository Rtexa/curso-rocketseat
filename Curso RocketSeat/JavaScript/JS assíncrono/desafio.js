//Exercicio 1
function sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
  }

function checaIdade(idade) {
    return new Promise(function (resolve, reject) {
        sleep(500).then(() => {
            if (idade > 18) {
                resolve();
            } else {
                reject();
            }
        });
    });
}
checaIdade(20)
    .then(function() {
        console.log("Maior que 18");
    })
    .catch(function() {
        console.log("Menor que 18");
    });
   
//Exercicio 2
var button = document.querySelector('#btn');
button.onclick = function() {
    var listElement = document.querySelector('#repolist');
    listElement.innerHTML = "";
    
    var li = document.createElement("li");
    li.appendChild(document.createTextNode('Carregando...'));
    listElement.appendChild(li);

    var user = document.querySelector('#github').value;
    axios.get('https://api.github.com/users/' + user + '/repos')
    .then(function(response) { 
        console.log(response)
        listElement.innerHTML = "";
        for (repo of response.data) {
            var li = document.createElement("li");
            li.setAttribute('id', repo.name);
            li.appendChild(document.createTextNode(repo.name));
            listElement.appendChild(li);
        }
    })
    .catch(function(error) {
        console.warn(error);
        alert('Erro na requisição');
        listElement.innerHTML = "";
    });
};